<?php

namespace Drupal\Tests\shortcode_video\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Tests the Drupal 8 shortcode video module functionality.
 *
 * @group shortcode
 */
class ShortcodeVideoTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['filter', 'shortcode', 'shortcode_video'];

  /**
   * The shortcode service.
   *
   * @var \Drupal\shortcode\ShortcodeService
   */
  private $shortcodeService;

  /**
   * Url of the site.
   *
   * @var \Drupal\Core\GeneratedUrl|string
   */
  private $siteUrl;

  /**
   * Perform any initial set up tasks that run before every test method.
   */
  public function setUp() {
    parent::setUp();
    $this->shortcodeService = $this->container->get('shortcode');
    $this->siteUrl = Url::fromRoute('<front>', [], ["absolute" => TRUE])->toString();
  }

  /**
   * Tests that the Video shortcode returns the right content.
   */
  public function testVideoShortcode() {

    $sets = [
      [
        'input' => '[video]https://www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  width="480" height="360" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video shortcode output matches.',
      ],
      [
        'input' => '[video]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  width="480" height="360" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with no https://',
      ],
      [
        'input' => '[video title="This is the title"]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="480" height="360" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with title',
      ],
      [
        'input' => '[video title="This is the title" class="ytclass"]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ytclass">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="480" height="360" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with title and class',
      ],
      [
        'input' => '[video title="This is the title" class="ytclass" width="600"]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ytclass">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="600" height="450" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with 600px width',
      ],
      [
        'input' => '[video title="This is the title" class="ytclass" height="600"]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ytclass">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="798" height="600" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with 600px height',
      ],
      [
        'input' => '[video title="This is the title" class="ytclass" width="900" height="700"]www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ytclass">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="900" height="700" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with width 800px height 600px',
      ],
      [
        'input' => '[video title="This is the title" class="ytclass" width="900" height="700"]//www.youtube.com/watch?v=RVMvART9kb8[/video]',
        'output' => '<div class="video-container ytclass">
    <iframe src="//www.youtube.com/embed/RVMvART9kb8"  title="This is the title"  width="900" height="700" frameborder="0" allowfullscreen></iframe>
</div>',
        'message' => 'Video with width 800px height 600px',
      ],
    ];

    foreach ($sets as $set) {
      $output = $this->shortcodeService->process($set['input']);
      //var_dump($output);
      $this->assertEqual($output, $set['output'], $set['message']);
    }
  }

}
