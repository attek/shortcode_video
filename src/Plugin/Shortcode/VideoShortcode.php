<?php

namespace Drupal\shortcode_video\Plugin\Shortcode;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Language\Language;
use Drupal\Core\Url;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * Embeds the video into the text.
 *
 * @Shortcode(
 *   id = "video",
 *   title = @Translation("Video macro"),
 *   description = @Translation("Embeds video into the content text.")
 * )
 */
class VideoShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process(array $attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    $attributes = $this->getAttributes([
      'class' => '',
      'width' => '',
      'height' => '',
      'showlink' => FALSE,
      'sub' => '',
      'forcesub' => FALSE,
      'title' => '',
    ],
      $attributes
    );

    $height = empty($attributes['height']) ? 0 : (int) ($attributes['height']);
    $width = empty($attributes['width']) ? 0 : (int) ($attributes['width']);

    // Set defaults.
    if (empty($width) && empty($height)) {
      $width = 480;
      $height = 360;
    }
    elseif ($width && empty($height)) {
      switch ($width) {
        case 450:
          $height = 315;
          break;

        case 480:
          $height = 360;
          break;

        case 640:
          $height = 480;
          break;

        case 960:
          $height = 720;
          break;

        default:
          $height = (int) ($width * 0.75);
          break;

      }
    }
    elseif ($height && empty($width)) {
      $width = (int) ($height * 1.33);
    }

    $attributes['height'] = $height;
    $attributes['width'] = $width;

    $url = UrlHelper::parse($text);

    // Get provider.
    $matches = [];
    preg_match('!(?:^.*\:\/\/|\/\/)?(www\.)?([^\?\/]*)(.*)$!', $url['path'], $matches);
    $provider = $matches[2];

    switch ($provider) {
      case 'youtu.be':
      case 'youtube.com':
        if (!empty($url['query']['v'])) {
          $params['video_id'] = $url['query']['v'];
          unset($params['v']);
          unset($url['query']['v']);
        }
        else {
          $id = trim($matches[3], '/');
          $id = str_replace('embed/', '', $id);
          $params['video_id'] = $id;
        }

        $embed_url = '//www.youtube.com/embed/';
        $params['attributes'] = $attributes;

        if (!empty($params['video_id'])) {
          if ($attributes['sub']) {
            $url['query']['hl'] = $attributes['sub'];
          }
          if ($attributes['forcesub']) {
            $url['query']['fs'] = 1;
          }
          if (!empty($url['query']['t'])) {
            $t = explode('m', $url['query']['t']);
            $start = (int) (trim($t[0])) * 60;
            $start += (int) ($t[1]);

            unset($url['query']['t']);
            $url['query']['start'] = $start;
          }
          $url['external'] = TRUE;

          $params['video_url'] = Url::fromUri($embed_url . $params['video_id'], $url)->toString();
        }
        break;

      case 'vimeo.com':
        $id = trim($matches[3], '/');
        $params['video_id'] = $id;
        $embed_url = '//player.vimeo.com/video/';
        $params['external'] = TRUE;

        if (!empty($params['video_id'])) {
          $params['attributes'] = $attributes;
          $params['video_url'] = Url::fromUri($embed_url . $params['video_id'])->toString();
        }
        break;

      default:
        if (!empty($text)) {
          // Default behaviour when no custom provider.
          $pattern = [
            '!http://!', '!https://!',
          ];
          $text = preg_replace($pattern, '//', $text);
          if (strpos($text, '//') === FALSE) {
            $text = '//' . $text;
          }
          $params['video_url'] = $text;

        }
        break;
    }

    // Build element attributes to be used in twig.
    $element_attributes = [
      'height' => $height,
      'width' => $width,
    ];

    if (!empty($attributes['class'])) {
      $element_attributes['class'] = $attributes['class'];
    }

    if (!empty($attributes['title'])) {
      $element_attributes['title'] = $attributes['title'];
    }

    // Filter away empty attributes.
    $element_attributes = array_filter($element_attributes);

    $output = [
      '#theme' => 'shortcode_video',
      '#url' => $params['video_url'],
      '#attributes' => $element_attributes,
      '#text' => $text,
    ];

    return $this->render($output);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[video]video url[/video]') . '</strong> ';
    if ($long) {
      $output[] = $this->t('Embeds video into the content text. With the <em>width</em> and <em>height</em> parameter you can specify the video size. Use <em>title</em> to specify a title attribute.') . '</p>';
      $output[] = '<p>' . $this->t('For YouTube videos you can specify the width with 420, 480, 640, 960 as the default videos sizes, then the height of the video will be added according to the default embed sizes. The default video size is 480x360px.') . '</p>';
    }
    else {
      $output[] = $this->t('Embeds the video into the text.') . '</p>';
    }

    return implode(' ', $output);
  }

}
